# Import Certificate data
data "aws_acm_certificate" "website_web" {
  domain   = "*.example.com"
  statuses = ["ISSUED"]
}

# NGINX Ingress Configuration
resource "helm_release" "nginx_ingress-web" {
  name = var.helm_ingress_nginx_release_name

  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = var.helm_ingress_nginx_chart_name
  version          = var.helm_ingress_nginx_version
  namespace        = var.ingress_nginx_namespace
  create_namespace = true
  values           = [file("eks-nginx-values.yaml")]

  depends_on = [
    module.eks
  ]

  # Provide extra configuration using annotations
  # Attach SSL Certificate to NLB
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-cert"
    value = data.aws_acm_certificate.website_web.id
  }
  # Add Subnet Source Ranges to allow incoming traffic
  set {
    name  = "controller.service.loadBalancerSourceRanges"
    value = "{${join(",", var.web_world_source_ranges)}}"
  }
}