provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "/Users/sameerkumar/.aws/credentials"
  profile                 = "default"
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "eks_eks-cluster-poc"
  }
}