module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "17.0.0"

  cluster_name    = var.cluster_name
  cluster_version = var.eks_version

  vpc_id  = module.vpc.vpc_id
  subnets = module.vpc.private_subnets

  # Create OpenID Connect Provider for EKS to enable IRSA (IAM Role Service Account)
  enable_irsa = true

  # Cluster Plane Configuration
  cluster_enabled_log_types     = var.cluster_enabled_logging
  cluster_log_retention_in_days = var.cluster_log_retention_in_days

  tags = var.default_tags

  node_groups = {
    eks_nodes_2 = {
      desired_capacity = 2
      min_capacity     = 1
      max_capacity     = 3

      instance_types   = [var.instance_type]

      # For "ami_release_version" Refer - https://docs.aws.amazon.com/eks/latest/userguide/eks-linux-ami-versions.html
      # Skip this attribute, if you want EKS to pick always the latest ami_release_version
      ami_release_version = var.ami_release_version

      launch_template_id      = aws_launch_template.small_nodegroup_lt.id
      launch_template_version = aws_launch_template.small_nodegroup_lt.default_version

      iam_role_arn = aws_iam_role.eks_worker_node_role.arn
      subnets      = module.vpc.private_subnets
      #source_security_group_ids = [""]			Please provide security group ids
      capacity_type = "ON_DEMAND"

      tags = var.default_tags
    }    
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
/*
module "eks_fargate" {
    source  = "terraform-module/eks-fargate-profile/aws"
    version = "2.2.6"

    cluster_name         = var.cluster_name
    subnet_ids           = module.vpc.private_subnets
    namespaces           = ["default"]
    labels = {
      "runon" = "fargate"
    }
}
*/
