variable "cluster_name" {
  type    = string
  default = "eks-cluster-poc"
}

variable "cluster_enabled_logging" {
  type = list(string)
  #default = ["api","audit","authenticator","controllerManager","scheduler"] # All possible cluster plane logging
  default = ["api", "controllerManager", "scheduler"]
}

variable "cluster_log_retention_in_days" {
  type    = number
  default = 30
}

variable "eks_version" {
  type    = string
  default = "1.20"
}

variable "instance_type" {
  default = "t3.medium"
  type    = string
}

variable "ami_release_version" {
  default = "1.20.4-20210519"
  type    = string
}

/*
variable "vpc_id" {
  type    = string
  default = "vpc-dbbf96bf"
}

variable "public_subnets" {
  type    = list(string)
  default = ["subnet-01cd02331ae8a7383", "subnet-02a61d6d067d57cef", "subnet-03f2c36cbf5f855a7"]
}

variable "private_subnets" {
  type    = list(string)
  default = ["subnet-58b7893c", "subnet-890538ff", "subnet-500a7008"]
}
*/

# Varibales for Tagging Resources
variable "instance_tags" {
  type = map(any)
  default = {
    "Name"             = "my-eks-workers"
    "environment-type" = "PRE-PRODUCTION",
    "resource-owner"   = "sam.tiku@gmail.com",
    "department-name"  = "technology-group"
  }
}

variable "default_tags" {
  type = map(any)
  default = {
    "environment-type" = "PRE-PRODUCTION",
    "resource-owner"   = "sam.tiku@gmail.com"
  }
}

# Variables for "nginx ingress controller" configuration
variable "helm_ingress_nginx_release_name" {
  type    = string
  default = "ingress-nginx"
}

variable "helm_ingress_nginx_chart_name" {
  type    = string
  default = "ingress-nginx"
}

variable "ingress_nginx_namespace" {
  type    = string
  default = "ingress-nginx"
}

variable "helm_ingress_nginx_version" {
  type    = string
  default = "3.30.0"
}

variable "ingress_nginx_web_class_name" {
  type    = string
  default = "nginx-web"
}

# Variables to configure IP Source Ranges
variable "web_world_source_ranges" {
  type = list(string)
  default = [
    "0.0.0.0/0",
    "212.22.45.67/27"
  ]
}