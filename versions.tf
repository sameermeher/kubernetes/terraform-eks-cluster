terraform {

  required_version = "=1.0.0"
  
  backend "s3" {
    bucket = "terraform-state-test"
    key    = "eks-cluster/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-state-test"
  }
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.40.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.1.0"
    }

    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }

    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.0.3"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.1.1"
    }
  }
}