# To test service internally by creating a temporary POD.
kubectl run curl-sameer --image=radial/busyboxplus:curl -i --tty --rm
curl http://<service-name>:<port>

# For SSL Offloading at NLB level - 
ports:
- name: https
    port: 443
    protocol: TCP
    targetPort: http (THIS NEEDS TO BE "HTTP")
# Test the Services with Urls
## Pre-Requisites - 
1. Attach Certificate to NLB
2. Update CNAME in your Domain Provider to redirect to NLB DNS
https://app1.sameermeher.xyz/
https://app2.sameermeher.xyz/

# TODO:
1. nginx-ingress tags are not applied in NLB, Target Group and other resources.... Need to check
2. Explore IngressClass configuration